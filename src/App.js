import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/Header';
import Contact from './components/Contact';
import Home from './components/Home';
import About from './components/About';


class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Contact />
        <Home />
        <About />
      </div>
    );
  }
}

export default App;